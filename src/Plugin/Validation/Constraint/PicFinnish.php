<?php

namespace Drupal\personal_id\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Provides an ID validity checker constraint.
 *
 * @Constraint(
 *   id = "PicFinnish",
 *   label = @Translation("Matches the pattern of Finnish Personal Identificatoin Code", context = "Validation"),
 *   type = "personal_id"
 * )
 */
class PicFinnish extends Constraint {

  /**
   * Error that will be shown if the provided value checksum is invalid.
   *
   * @var string
   */
  public $errorMessage = 'The provided value "%value" does not match the format of Finnish Personal Identification Code.';

  /**
   * Error that will be shown if the provided value checksum is invalid.
   *
   * @var string
   */
  public $errorLengthMismatch = 'The length of "%value" length does not match match the format of Finnish Personal Identification code. Valid Finnish Personal Identification Code length is %length characters.';

  /**
   * Error that will be shown if the provided value contains lowercase chars.
   *
   * @var string
   */
  public $errorNotCapitalized = 'The provided value "%value" is not fully capitalized. Finnish Personal Identification Code has only numbers and uppercase characters, if any.';

}
