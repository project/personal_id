<?php

namespace Drupal\personal_id\Plugin\Validation\Constraint;

use Drupal\personal_id\Plugin\Field\FieldType\PersonalIdItem;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the ID validity checker constraint.
 */
class PicFinnishValidator extends ConstraintValidator {

  /**
   * Regex pattern for validation.
   *
   * @var string
   */
  public $pattern = '/^(0[1-9]|[1-2][0-9]|3[0-1])(0[1-9]|1[0-9])([0-9]{2})([-+A])([0-9]{3})([0-9A-Y])$/';

  /**
   * List of valid check sum characters.
   *
   * @var string
   */
  public $checksumChars = '0123456789ABCDEFHJKLMNPRSTUVWXY';

  /**
   * Length of valid Finnish PIC.
   *
   * @var int
   */
  public $length = 11;

  /**
   * {@inheritdoc}
   */
  public function validate($items, Constraint $constraint) {

    if (!$constraint instanceof PicFinnish) {
      throw new UnexpectedTypeException($constraint, __NAMESPACE__ . '\\PicFinnish');
    }

    if (!empty($items)) {
      foreach ($items as $item) {
        if (!$item instanceof PersonalIdItem) {
          throw new UnexpectedTypeException($item, __NAMESPACE__ . '\\PersonalIdItem');
        }
        $this->validateString($item->value, $constraint);
      }

    }
  }

  /**
   * FinnishPic validator.
   *
   * @param string $value
   *   Value to be validated.
   * @param \Symfony\Component\Validator\Constraint $constraint
   *   The constraint holding the messages.
   */
  public function validateString($value, Constraint $constraint) {
    $trimmed = trim($value);

    if (strlen($trimmed) != $this->length) {
      $this->context->addViolation($constraint->errorLengthMismatch, [
        '%value' => $value,
        '%length' => $this->length,
      ]);
      return;
    }

    // If string is not capitalized checksum validation fails as well, so we can
    // stop in the first check, no need to create more validation errors.
    if ($trimmed !== strtoupper($trimmed)) {
      $this->context->addViolation($constraint->errorNotCapitalized, [
        '%value' => $value,
      ]);
      return;
    }

    // Validate value against PIC (FIN) pattern.
    $components = [];
    preg_match($this->pattern, $trimmed, $components);

    /*
     * First check value matches pattern.
     * Then if pattern matches validate checksum character (last character)
     *
     * @see https://en.wikipedia.org/wiki/National_identification_number#Finland
     */
    if ($components) {
      $combined = $components[1] . $components[2] . $components[3] . $components[5];
      $calculated_checksum_char = $this->checksumChars[$combined % 31];
    }

    if (empty($components) || $components[6] != $calculated_checksum_char) {
      $this->context->addViolation($constraint->errorMessage, [
        '%value' => $value,
      ]);
    }
  }

}
