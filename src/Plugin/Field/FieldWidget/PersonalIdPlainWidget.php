<?php

namespace Drupal\personal_id\Plugin\Field\FieldWidget;

use Drupal\Core\Field\Plugin\Field\FieldWidget\StringTextfieldWidget;

/**
 * Defines the 'personal_id' field widget.
 *
 * @FieldWidget(
 *   id = "personal_id_plain_widget",
 *   label = @Translation("Textfield"),
 *   field_types = {"personal_id"},
 * )
 */
class PersonalIdPlainWidget extends StringTextfieldWidget {

}
