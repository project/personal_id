<?php

namespace Drupal\personal_id\Plugin\Field\FieldType;

// die('https://www.drupal.org/docs/8/api/entity-api/entity-validation-api/providing-a-custom-validation-constraint');
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\StringItem;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Defines the 'personal_id' field type.
 *
 * @FieldType(
 *   id = "personal_id",
 *   label = @Translation("Personal ID"),
 *   description = @Translation("A field containing a Personal ID."),
 *   category = @Translation("Text"),
 *   default_widget = "personal_id_plain_widget",
 *   default_formatter = "basic_personal_id_string"
 * )
 */
class PersonalIdItem extends StringItem {

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return [
      'selected_validations' => [],
    ] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = parent::propertyDefinitions($field_definition);
    $properties['value']
      ->setLabel(new TranslatableMarkup('Personal ID Field'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::fieldSettingsForm($form, $form_state);

    $settings = $this->getSettings();
    $constraint_manager = $this->getTypedDataManager()
      ->getValidationConstraintManager();

    $all_constraints = $constraint_manager->getDefinitionsByType('personal_id');
    $options = array_filter($all_constraints, function ($value, $key) {
      return isset($value['provider']) && $value['provider'] === 'personal_id';
    }, ARRAY_FILTER_USE_BOTH);

    array_walk($options, function (&$value) {
      $value = $value['label'];
    });

    $element['selected_validations'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Personal ID validation'),
      '#options' => $options,
      '#default_value' => $settings['selected_validations'],
      '#description' => $this->t('Select which patterns are allowed. <strong>WARNING!</strong> If you select more than one option here all of them must be met to pass the validation.'),
    ];

    return $element;
  }

}
