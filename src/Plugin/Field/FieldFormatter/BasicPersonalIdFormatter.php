<?php

namespace Drupal\personal_id\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\Plugin\Field\FieldFormatter\BasicStringFormatter;

/**
 * Plugin implementation of the 'string' formatter.
 *
 * @FieldFormatter(
 *   id = "basic_personal_id_string",
 *   label = @Translation("Plain text"),
 *   field_types = {
 *     "personal_id",
 *   },
 *   quickedit = {
 *     "editor" = "plain_text"
 *   }
 * )
 */
class BasicPersonalIdFormatter extends BasicStringFormatter {

}
