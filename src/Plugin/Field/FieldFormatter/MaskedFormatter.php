<?php

namespace Drupal\personal_id\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\Plugin\Field\FieldFormatter\BasicStringFormatter;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'string' formatter.
 *
 * @FieldFormatter(
 *   id = "personal_id_masked",
 *   label = @Translation("Masked text"),
 *   field_types = {
 *     "personal_id",
 *   }
 * )
 */
class MaskedFormatter extends BasicStringFormatter {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $options = parent::defaultSettings();

    $options['mask_char'] = '*';
    $options['mask_length'] = '-';
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $form['mask_char'] = [
      '#type' => 'textfield',
      '#size' => 1,
      '#title' => $this->t('Mask the value using this character.'),
      '#required' => TRUE,
      '#default_value' => $this->getSetting('mask_char'),
    ];
    $form['mask_length'] = [
      '#type' => 'select',
      '#title' => $this->t('Use fixed string length when masked.'),
      '#options' => $this->getMaskLengthOptions(),
      '#required' => TRUE,
      '#default_value' => $this->getSetting('mask_length'),
    ];

    return $form;
  }

  /**
   * Provide a list of available Mask length options.
   *
   * @return array
   *   Keyed by length (FALSE = no change), labels as values.
   */
  public function getMaskLengthOptions() {
    return [
      '-' => $this->t('Keep original string length'),
      '5' => $this->t('Use fixed length of %length characters', [
        '%length' => 5,
      ]),
      '10' => $this->t('Use fixed length of %length characters', [
        '%length' => 10,
      ]),
      '15' => $this->t('Use fixed length of %length characters', [
        '%length' => 15,
      ]),
      '20' => $this->t('Use fixed length of %length characters', [
        '%length' => 20,
      ]),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    if ($this->getSetting('mask_char')) {
      $summary[] = $this->t('Mask character: @char.', [
        '@char' => $this->getSetting('mask_char'),
      ]);
    }
    if ($this->getSetting('mask_length') != '-') {
      $summary[] = $this->t('Change the length to @char characters.', [
        '@char' => $this->getSetting('mask_length'),
      ]);
    }
    else {
      $summary[] = $this->t('Keep the length unchanged.');
    }
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  protected function viewValue(FieldItemInterface $item) {
    // The text value has no text format assigned to it, so the user input
    // should equal the output, including newlines.
    $char = $this->getSetting('mask_char');
    $length = $this->getSetting('mask_length');
    if ($length && $length != '-') {
      $length = strlen($item->getValue());
    }

    $value = str_pad('', $length, $char);
    return [
      '#type' => 'inline_template',
      '#template' => '{{ value|nl2br }}',
      '#context' => ['value' => $value],
    ];
  }

}
