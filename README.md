# Personal ID

This module adds a new field type to Drupal with optional pattern
validation to match Personal Identification Code (PIC) *format*. This
module does not check if the given PIC is a real or fake, but considers 
only the format.

Currently supported format checks include:

* Finnish Personal Ideentification code
  [https://en.wikipedia.org/wiki/National_identification_number#Finland](https://en.wikipedia.org/wiki/National_identification_number#Finland)

Adding more validators is a matter of providing constraint and the
validator, see examples in
[the project Git repository](https://git.drupalcode.org/project/personal_id/tree/8.x-1.x/src/Plugin/Validation/Constraint)

**Patches for the new constraints and validators are greatly
appreciated!**

## Installation

1. Download the module, `compser require drupal/personal_id`.
2. Enable Personal ID -module (`personal_id`).
3. Add a field of type "Personal ID" to an entity - it is grouped with
   other Text fields. Save field settings.
4. In the "[Field name] settings for [bundle name]" page select which
   "Personal ID validation" you wish to have.
5. Note that **ALL** selected validators must pass for user to be able
   to submit value in the field.

## Credits

Personal ID module is mantained by:

* Perttu Ehn - https://drupal.org/u/rpsu
